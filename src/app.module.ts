import { Module } from '@nestjs/common';
//import { AppController } from './app.controller';
//import { AppService } from './app.service';
import { TasksModule } from './tasks/tasks.module';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TasksModule,
  TypeOrmModule.forRoot({
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: 'wasan@13181',
    database: 'task',
    autoLoadEntities: true,
    synchronize: true
  })
],
  //,
 // controllers: [AppController],
 // providers: [AppService],
})
export class AppModule {}
